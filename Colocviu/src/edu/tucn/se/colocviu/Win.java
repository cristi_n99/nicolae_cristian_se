package edu.tucn.se.colocviu;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Win extends JFrame {

    private String path;

    Win() {
        super("GUI");
        JTextField textField = new JTextField(1000);
        textField.setMaximumSize(new Dimension(1000, 100));
        add(textField);


        JTextArea textArea = new JTextArea();

        JButton button = new JButton("Show the text");
        button.addActionListener(e -> {
            path = "C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Colocviu\\src\\edu\\tucn\\se\\colocviu\\" + textField.getText();
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
                StringBuilder stringBuilder = new StringBuilder();
                String continut;
                while ((continut = bufferedReader.readLine()) != null) {
                    stringBuilder.append(continut).append('\n');
                }
                textArea.setText(String.valueOf(stringBuilder));
            } catch (FileNotFoundException ex) {
                textArea.setText("The file doesn't exist!");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });


        add(button);
        add(textArea);
        textArea.setEditable(false);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);
    }

}