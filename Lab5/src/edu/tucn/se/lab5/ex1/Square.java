package edu.tucn.se.lab5.ex1;

public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);

    }

    public Square(double side, String c, boolean f) {
        super(side, side, c, f);

    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public double getSide() {
        return this.getLength();
    }

    @Override
    public double getWidth() {
        return super.getWidth();
    }

    @Override
    public double getLength() {
        return super.getLength();
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }

    @Override
    public String toString() {
        return "A square of side=" + getSide() + " which is a subclass of " + super.toString();
    }
}
