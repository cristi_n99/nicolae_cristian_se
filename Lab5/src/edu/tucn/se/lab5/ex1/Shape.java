package edu.tucn.se.lab5.ex1;

abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
        color = "red";
        filled = true;
    }

    public Shape(String c, boolean f) {
        color = c;
        filled = f;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    abstract double getArea();
    abstract double getPerimeter();

    @Override
    public String toString() {
        if (isFilled())
            return "A Shape of color " + getColor() + " and filled";
        else
            return "A Shape of color " + getColor() + " and not filled";
    }
}
