package edu.tucn.se.lab5.ex3;

public abstract class Sensor {
    private String location;
    public abstract int readValue();

}
