package edu.tucn.se.lab5.ex4;



import java.util.Random;

public class LightSensor extends Sensor {

    public int readValue()
    {
        Random random=new Random();
        return random.nextInt(101);
    }
}
