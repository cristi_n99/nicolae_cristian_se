package edu.tucn.se.lab5.ex2;

public class ProxyImage implements Image {

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if (fileName.endsWith("_r")) {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
                rotatedImage.display();
            }} else {
                if (realImage == null) {
                    realImage = new RealImage(fileName);
                }
                realImage.display();
            }

    }}