package edu.tucn.se.lab5.ex2;

public class Test {
    public static void main(String[] args) {
        RealImage i1=new RealImage("submodul.png");
        ProxyImage p1=new ProxyImage("ssss");
        p1.display();
        ProxyImage p2=new ProxyImage("ssss_r");
        p2.display();
    }
}
