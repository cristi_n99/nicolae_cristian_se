package edu.tucn.se.ex3;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int continua = 1;
        Scanner scanner = new Scanner(System.in);
        while (continua != 0) {
            System.out.println("Enter filename without ext:");
            String filename = scanner.next();
            System.out.println("Choose an action");
            System.out.println("1.Encrypt");
            System.out.println("2.Decrypt");
            int alegere = scanner.nextInt();
            switch (alegere) {
                case 1:
                    encrypt(filename);
                    break;

                case 2:
                    decrypt(filename);
                    break;

            }
        }
    }

    public static void encrypt(String filename) {
        try {
            FileReader infile = new FileReader("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex3\\" + filename + ".txt");
            FileWriter outfile = new FileWriter("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex3\\" + filename + ".enc");
            int ch;
            while ((ch = infile.read()) != -1) {
                outfile.append((char) (ch << 1));
            }

            infile.close();
            outfile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void decrypt(String filename) {
        try {
            FileReader infile = new FileReader("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex3\\" + filename + ".enc");
            FileWriter outfile = new FileWriter("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex3\\" + filename + ".dec");
            int ch;
            while ((ch = infile.read()) != -1) {
                outfile.append((char) (ch >> 1));
            }

            infile.close();
            outfile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}