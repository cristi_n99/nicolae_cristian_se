package edu.tucn.se.ex4;

import java.io.*;

public class Car implements Serializable {
    private String model;
    private double price;

    public Car()
    {
        this.model="Default";
        this.price=0.0;
    }
    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    public void SaveCar() {
        try {
            FileOutputStream fileout = new FileOutputStream("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex4\\masini\\" + model + ".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(this);
            out.close();
            fileout.close();
            System.out.println("Car saved");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void displayCars() {
        File dir = new File("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex4\\masini\\");
        for(File f : dir.listFiles()){
            System.out.println(f.getName());
        }
    }

    public static Car readModel(String model) {
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex4\\masini\\" + model + ".ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Car car = (Car) in.readObject();
            in.close();
            fileIn.close();
            return car;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

}
