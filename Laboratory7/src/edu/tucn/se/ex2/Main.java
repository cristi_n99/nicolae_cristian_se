package edu.tucn.se.ex2;

import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BufferedReader input = null;
        String currentDir = System.getProperty("user.dir");

        try {
            input = new BufferedReader(new FileReader("C:\\Users\\nicol\\Desktop\\An2Sem2\\SE\\nicolae_cristian_se\\Laboratory7\\src\\edu\\tucn\\se\\ex2\\text.txt"));
            int charCount = 0;
            int character;
            System.out.println("Insert a character: ");
            char c = scanner.next().charAt(0);
            while ((character = input.read()) != -1) {
                if ((char) character == c) charCount++;
            }
            System.out.println("Numarul de aparitii ale caracterului " + c + " este " + charCount);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
