package edu.tucn.se.ex2;
import javax.swing.*;
import java.awt.*;

public class Win extends JFrame {
    private int apasari = 0;

    public Win() {
        super("Ex2");
        super.setLayout(null);
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        super.setSize(550, 500);

        JPanel panel = new JPanel();
        JTextField textField2 =new JTextField(10);
        JTextField textField = new JTextField(5);

        JButton button = new JButton("Apasa");
        button.addActionListener(e -> textField.setText(""+ apasari++));
        button.addActionListener(e -> textField2.setText("Coie"));

        panel.setVisible(true);
        add(panel);
        add(textField);
        add(textField2);
        textField.setBounds(30, 70, 200, 20);
        textField2.setBounds(30, 90, 200, 20);
        button.setBounds(60, 140 ,100 ,20);
        add(button);
        setVisible(true);
    }
}