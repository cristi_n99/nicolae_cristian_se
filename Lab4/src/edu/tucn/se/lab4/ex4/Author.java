package edu.tucn.se.lab4.ex4;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author() {
        name = "cristi";
        email = "werqwq@rrrr";
        gender = 'm';
    }

    public Author(String n) {
        name = n;
        email = "cristi@ysssssahoo.com";
        gender = 'M';
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Name: '" + this.name + "(" + this.gender + ") at " + this.email;
    }
}

