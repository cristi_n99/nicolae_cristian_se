package edu.tucn.se.lab4.ex4;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        Author[] a=new Author[3];
        for(int i=0; i<3; i++)
        {
            a[i]=new Author();
        }

        Book b1=new Book("book1",a, 50.0 );
        System.out.println(b1.toString());
        b1.printAuthors();
    }
}
