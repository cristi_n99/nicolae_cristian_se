package edu.tucn.se.lab4.ex4;



public class Book {
    private Author[] author;
    private double price;
    private int qtyInStock;
    private String name;
    public Book(String n,Author[] authors, double p)
    {
        this.name=n;
        this.price=p;
        this.author=authors;
    }
    public Book(String n,Author[] authors, int qty)
    {
        this.name=n;
        this.qtyInStock=qty;
        this.author=authors;
    }

    public Author[] getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }
    public void printAuthors()
    {
        for(int i=0; i<author.length;i++)
        {
            System.out.println(author[i].getName());
        }
    }
    @Override
    public String toString()
    {
        return getName()+" by "+ author.length+ " authors";
    }
}
