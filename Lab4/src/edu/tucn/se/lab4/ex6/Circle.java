package edu.tucn.se.lab4.ex6;

public class Circle extends Shape {
    private double radius;

    public Circle() {
        radius = 1.0;

    }

    public Circle(double r) {
        radius = r;
    }

    public Circle(double r, String c, boolean f) {
        super(c, f);
        radius = r;

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return radius * Math.PI * Math.PI;
    }

    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "A circle of radius=" + this.radius + " which is a subclass of " + super.toString();
    }
}
