package edu.tucn.se.lab4.ex6;

public class Test {
    public static void main(String[] args) {
        Circle c1 = new Circle(5, "red", true);
        System.out.println(c1.toString());
        Square s1 = new Square(5.0);
        System.out.println(s1.toString());
        s1.setLength(15.0);
        System.out.println(s1.toString());
        s1.setWidth(20);
        System.out.println(s1.toString());
        Square s2 = new Square(50, "blue", false);
        System.out.println(s2.toString());
    }
}
