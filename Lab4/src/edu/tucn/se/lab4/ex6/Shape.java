package edu.tucn.se.lab4.ex6;

public class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        color = "red";
        filled = true;
    }

    public Shape(String c, boolean f) {
        color = c;
        filled = f;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        if (isFilled())
            return "A Shape of color " + getColor() + " and filled";
        else
            return "A Shape of color " + getColor() + " and not filled";
    }
}
