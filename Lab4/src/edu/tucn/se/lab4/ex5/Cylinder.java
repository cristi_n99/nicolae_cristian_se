package edu.tucn.se.lab4.ex5;

public class Cylinder extends Circle {
    private double height;
    public Cylinder()
    {
        height=1.0;
    }
    public Cylinder(double r)
    {
        super(r);
        height=1.0;
    }
    public Cylinder(double r, double h)
    {
        super(r);
        height=h;
    }

    public double getHeight() {
        return height;
    }
    public double getVolume()
    {
        return getArea()*getHeight();
    }
}
