package edu.tucn.se.lab4.ex5;

public class Test {
    public static void main(String[] args) {
        Circle c1=new Circle();
        System.out.println(c1.toString());
        Cylinder cy1=new Cylinder(5);
        System.out.println("Cylinder of volume: "+cy1.getVolume());
    }
}
