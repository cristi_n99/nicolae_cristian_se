package edu.tucn.se.lab4.ex3;

public class Book {
    private Author author;
    private double price;
    private int qtyInStock;
    private String name;
    public Book(String n,Author author, double p)
    {
        this.price=price;
        this.name=n;

    }
    public Book(String n,Author author, int qty)
    {
        this.name=n;
        this.qtyInStock=qty;
    }

    public Author getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }
    @Override
    public String toString()
    {
        return getName()+"by "+author.getName()+"("+author.getGender()+") at "+author.getEmail();
    }
}
