package edu.tucn.se.lab3.ex3;

public class Author {
    String name;
    String email;
    char gender;
    public Author(String n, String e, char g)
    {
        name=n;
        email=e;
        gender=g;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
    void authorString()
    {
        System.out.println(getName()+"("+getGender()+") at "+getEmail());
    }
}
