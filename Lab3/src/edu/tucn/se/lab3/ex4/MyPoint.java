package edu.tucn.se.lab3.ex4;
import java.lang.Math;

public class MyPoint {
    int x;
    int y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(int a, int b) {
        x = a;
        y = b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    void setXY(int a, int b) {
        x = a;
        y = b;
    }
    void pointString()
    {
        System.out.println("("+x+","+y+")");
    }
   void distanceTo(int a, int b)
    {
        int c=Math.abs((x-a)^2+(y-b)^2);

        System.out.printf("Distance is: %f ",Math.sqrt(c));
    }
    void distanceTo(MyPoint p)
    {
        int c=Math.abs((x-p.x)^2+(y-p.y)^2);

        System.out.printf("Distance is: %f ",Math.sqrt(c));
    }
}
