package edu.tucn.se.lab3.ex2;

public class Circle {
    double radius;
    String color;
    public Circle()
    {
        radius=1.0;
        color="red";
    }
    public Circle(double r, String c)
    {
        radius=r;
        color=c;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius*Math.PI*Math.PI;
    }
}
