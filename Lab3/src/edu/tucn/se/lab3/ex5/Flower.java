package edu.tucn.se.lab3.ex5;

public class Flower{
    int petal;
    static int count;
    Flower(){
        count++;
        System.out.println("Flower has been created!");
    }
    static void counter()
    {
        System.out.println("Number of objects created: " + count);
    }


    public static void main(String[] args) {
        Flower[] garden = new Flower[5];

        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;

        }
    counter();
    }
}