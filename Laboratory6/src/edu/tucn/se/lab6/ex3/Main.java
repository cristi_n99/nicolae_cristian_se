package edu.tucn.se.lab6.ex3;



public class Main {

    public static void main(String[] args) {
        Bank bank=new Bank();
        bank.addAccount("Varga", 5000);
        bank.addAccount("Popica", 1200);
        bank.addAccount("Dioane", 499);
        bank.addAccount("Cristi", 10000);
        bank.printAccounts();
        System.out.println(" ");

        bank.printAccounts(499, 5000);

        System.out.println("\n");

        for(BankAccount i: bank.accounts)
            System.out.println(i.toString());
    }
}
