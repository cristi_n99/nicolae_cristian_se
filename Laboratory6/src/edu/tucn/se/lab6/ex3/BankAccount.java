package edu.tucn.se.lab6.ex3;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public String getOwner() {
        return owner;

    }

    public double getBalance() {
        return balance;
    }

    void withDraw(double balance)
    {this.balance-=balance;}
    void deposit(double balance)
    {this.balance+=balance;}


    @Override
    public boolean equals(Object object) {
        if (object instanceof BankAccount) {
           BankAccount bankAccount = (BankAccount) object;
            return balance == bankAccount.balance;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
