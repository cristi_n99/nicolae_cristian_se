package edu.tucn.se.lab6.ex3;



import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

public class Bank {
    TreeSet<BankAccount> accounts=new TreeSet<BankAccount>(new Comp());

    public void addAccount(String owner, double balance)
    {
        BankAccount bankAccount=new BankAccount(owner, balance);
        accounts.add(bankAccount);
    }

    void printAccounts()
    {
        //Collections.sort(accounts, new Comp());
        for(BankAccount i:accounts)
        {
            System.out.println(i.toString());
        }
    }

    void printAccounts(double minBalance, double maxBalance)
    {
       for(BankAccount i: accounts)
       {
           if(i.getBalance()>=minBalance && i.getBalance()<=maxBalance)
               System.out.println(i.toString());
       }
    }
}
