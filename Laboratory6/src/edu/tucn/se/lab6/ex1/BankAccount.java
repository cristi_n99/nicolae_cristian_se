package edu.tucn.se.lab6.ex1;
import java.util.Objects;
public class BankAccount {
    private String owner;
    private double balance;

    BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    void withDraw(double balance)
    {
        this.balance-=balance;
    }
    void Deposit(double balance)
    {
        this.balance+=balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount p = (BankAccount) obj;
            return balance == p.balance;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
