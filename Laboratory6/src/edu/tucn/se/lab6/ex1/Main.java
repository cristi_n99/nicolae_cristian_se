package edu.tucn.se.lab6.ex1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        List<BankAccount> list1=new ArrayList<>();

        BankAccount p1=new BankAccount("Cristi", 556.0);
        BankAccount p2=new BankAccount("Varga", 678.0);
        BankAccount p3=new BankAccount("Dio", 878.0);
        BankAccount p4=new BankAccount("Dio", 878.0);
        list1.add(p1);
        list1.add(p2);
        list1.add(p3);
        list1.add(p4);


        Iterator<BankAccount> it = list1.iterator();

        while (it.hasNext()) {
            String name = (String) it.next().getOwner();
            System.out.println(name);
        }
        for (int i = 0; i < list1.size(); i++) {
            if (i == list1.size() - 1) break;
            System.out.println(
                    "comparison between "
                            + i
                            + " and "
                            + (int) (i + 1)
                            + " -> "
                            + list1.get(i).equals(list1.get(i + 1))
            );
            System.out.println(list1.get(i).hashCode());
        }
    }



}
