package edu.tucn.se.lab6.ex4;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Dictionary dictionary = new Dictionary();
        String w = scanner.nextLine();
        String d = scanner.nextLine();

        Word cuvant = new Word(w);
        Definition def = new Definition(d);

        dictionary.addWord(cuvant, def);
        dictionary.getAllWords();
        dictionary.getDefinition(cuvant);

    }
}
