package edu.tucn.se.lab6.ex4;

public class Definition {
    private String definition;

    public Definition(String definition)
    {
        this.definition=definition;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
