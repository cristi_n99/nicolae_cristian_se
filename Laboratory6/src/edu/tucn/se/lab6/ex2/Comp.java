package edu.tucn.se.lab6.ex2;

import java.util.Comparator;

public class Comp implements Comparator<BankAccount> {

    public int compare(BankAccount x1, BankAccount x2) {
        return (int) x1.getOwner().compareTo(x2.getOwner());

    }
}

