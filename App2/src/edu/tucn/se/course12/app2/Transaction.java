package edu.tucn.se.course12.app2;

public class Transaction {
    private int amount;
    Account accountReceiver;
    Account accountDeliver;

    Transaction(Account accountReceiver, Account accountDeliver, int amount)
    {
        this.accountDeliver=accountDeliver;
        this.accountReceiver=accountReceiver;
        this.amount=amount;
    }
}
