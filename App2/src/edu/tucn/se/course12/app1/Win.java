package edu.tucn.se.course12.app1;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class Win extends JFrame {
    private String path;

    Win()
    {
        setSize(400,400);
        setLayout(new GridLayout(3,1));
        JButton button1=new JButton("Write!");
        JTextField textField1=new JTextField();
        JTextField textField2=new JTextField();

        button1.addActionListener(e->{
            path=textField1.getText();
            try
            {
                BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter(path, true));
                bufferedWriter.append(textField2.getText());

                bufferedWriter.close();
                textField2.setText("");

            }  catch (IOException ex) {
                ex.printStackTrace();
            } ;


        });

        add(button1);
        add(textField1);
        add(textField2);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
