package edu.tucn.se.course12.aplicatielogin;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class Win extends JFrame {
    HashMap<String, String> database = new HashMap<>();
    private String user;
    private String password;

    Win()
    {
        setSize(400, 400);
        setLayout(new GridLayout(3, 2));
        JLabel label1=new JLabel("User");
        JLabel label2=new JLabel("Password");
        JButton button1=new JButton("Save");
        JButton button2=new JButton("Login");
        JTextField textField1=new JTextField();
        JTextField textField2=new JTextField();
        textField1.setText("Login");
        textField2.setText("Password");
        button1.addActionListener(e->{
            String s1=textField1.getText();
            String s2= textField2.getText();
            database.put(s1,s2);
            textField1.setText("");
            textField2.setText("");
        });
        button2.addActionListener(e->{
            if(database.containsKey(textField1.getText()) && textField2.getText().equals(database.get(textField1.getText())))
            {
                System.out.println("User confirmed");
            }
            else
            {
                System.out.println("User not registered");
            }
        });




        add(label1);


        add(textField1);
        add(label2);
        add(textField2);
        add(button1);
        add(button2);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
